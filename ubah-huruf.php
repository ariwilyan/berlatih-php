<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Huruf</title>
</head>
<body>
    <?php
        function ubah_huruf($string){
            //kode di sini
            $abjad = "abcdefghijklmnopqrstuvwxyz";
            $tampung = "";
            for ($i=0; $i<strlen($string); $i++) {
                for ($j=0; $j<strlen($abjad)-1; $j++) {
                    if ($string[$i] == $abjad[$j]) {
                        $tampung .= $abjad[$j+1];
                    } else if ($string[$i] == $abjad[strlen($abjad)-1]) {
                        $tampung .= $abjad[0];
                        break;
                    }
                }
            }
            return $tampung . '<br>';
        }
            
        // TEST CASES
        echo ubah_huruf('wow'); // xpx
        echo ubah_huruf('developer'); // efwfmpqfs
        echo ubah_huruf('laravel'); // mbsbwfm
        echo ubah_huruf('keren'); // lfsfo
        echo ubah_huruf('semangat'); // tfnbohbu
    ?>
</body>
</html>